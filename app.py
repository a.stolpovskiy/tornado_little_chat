from tornado import websocket, web, ioloop


class MessageBuffer(object):
    def __init__(self):
        self.waiters = set()
        self.messages = []


class ChatHandler(web.RequestHandler):
    def get(self):
        print(global_message_buffer.messages)
        self.render('pages/chat_room.html',
                    messages=global_message_buffer.messages)


class SocketHandler(websocket.WebSocketHandler):
    def open(self):
        print("WebSocket opened")
        global_message_buffer.waiters.add(self)

    def on_message(self, message):
        global_message_buffer.messages.append(message)
        del_waiters = []
        for waiter in global_message_buffer.waiters:
            try:
                waiter.write_message(message)
            except websocket.WebSocketClosedError:
                del_waiters.append(waiter)
        for waiter in del_waiters:
            global_message_buffer.waiters.remove(waiter)

    def on_close(self):
        print("WebSocket closed")

    def check_origin(self, origin):
        return True


app = web.Application(
    [
        (r'/', ChatHandler),
        (r'/ws', SocketHandler),
    ],
    template_path='templates',
    static_path='static',
)


if __name__ == '__main__':
    global_message_buffer = MessageBuffer()
    app.listen(8888, address='127.0.0.1')
ioloop.IOLoop.instance().start()
