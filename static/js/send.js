var ws = new WebSocket("ws://localhost:8888/ws");
ws.onmessage = function (evt) {
     $( "#scroll" ).append("<div>" + evt.data + "</div>");
     $( "#messages" ).animate({scrollTop: $( "#scroll" ).height()}, 300);
  };


$('#send').submit(function(){
    msg = $( "#msg" ).val();
    name = $( "#name" ).val();
    $( "#msg" ).val('');
    ws.send(name + ": " + msg);
    
    return false;
});